### <img src="https://media.giphy.com/media/hvRJCLFzcasrR4ia7z/giphy.gif" width="25px"> Hello World, I'm saka2jp!

<a href="https://visitorbadge.io/status?path=https%3A%2F%2Fgitlab.com%2Fsaka2jp">
    <img align="right" 
      src="https://api.visitorbadge.io/api/visitors?path=https%3A%2F%2Fgitlab.com%2Fsaka2jp&labelColor=%23697689&countColor=%23fc6d26"
    />
</a>

<a href="https://www.linkedin.com/in/saka2jp/"><img align="left" src="https://img.shields.io/badge/LinkedIn-0A66C2?&style=for-the-badge&logo=LinkedIn&logoColor=white" /></a>
<a href="https://twitter.com/saka2jp"><img align="left" src="https://img.shields.io/badge/Twitter-1DA1F2?&style=for-the-badge&logo=Twitter&logoColor=white" /></a>
<a href="mailto:saka2jp@gmail.com"><img align="left" src="https://img.shields.io/badge/Email-EA4335?&style=for-the-badge&logo=Gmail&logoColor=white" /></a>

<br/><br/>

**About me**

- 💼 Software Developer in Japan 🇯🇵

- 📝 My flagship [blog](https://jumpyoshim.hatenablog.com/entry/how-to-implement-python-code-with-high-maintainability-and-readability), 400+ bookmark on Hatena

- ❤️ I love writing Golang, TypeScript

**Skills**

[![Skills](https://skillicons.dev/icons?i=go,ts,js,python,django,react,vue,nextjs,nuxtjs,gatsby,graphql,mysql,redis,docker,kubernetes,grafana,aws,gcp,firebase,github,githubactions,gitlab,netlify,vscode)](https://skillicons.dev)

**Badges**

<a href="https://www.credly.com/badges/8a783bd7-251c-4fb0-bd3b-7d7322e1d823/public_url">
    <img height="50" alt="aws-saa" src="https://images.credly.com/size/110x110/images/0e284c3f-5164-4b21-8660-0d84737941bc/image.png">
</a>

![saka2jp's gitlab stats](https://gitlab-readme-stats-opal.vercel.app/api?remote_username=pmarshall&remote_gitlab=gitlab.quantummetric.com&combine_remote_and_public=true&username=saka2jp)